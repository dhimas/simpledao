# the usage as easy as import the library
import simpledao

simpledao.init({
    "user": "postgres",
    "password": "Manis001",
    "host": "127.0.0.1",
    "port": "5432",
    "database": "testorm",
})

# table_name|primary_key
# to accomodate non 'id' primary key
book = simpledao.env["book|book_id"]
# lookup / search data with domain and required fields
res = book.search([
    ("author", '=', "dhimas")
], ["title", "author"], limit=1, offset=0)
print("search limit 1 =", res)

res = book.search([
    ("author", '=', "dhimas")
], ["title", "author"])
print("search =", res)
# count
res = book.search_count([
    ("author", '=', "dhimas")
], limit=1, offset=0)
print("search_count limit 1 =", res)
res = book.search_count([
    ("author", '=', "dhimas")
])
print("search_count =", res)
# list of vals, for multiple create
ids = book.create([{
    "title": "test",  
    "author": "test2",
}])
print("create =", ids)
# list of id as ids = [] multiple write
# return exception if fail
book.write(ids, {
    "author": "dhimas"
})
# advance query
simpledao.cr.execute("select * from book")
print("direct query select =", simpledao.cr.fetchall())
# unlink throw exception if fail
book.unlink(ids)
# direct query unlink
simpledao.cr.execute("delete from book where true")
simpledao.cr.commit()