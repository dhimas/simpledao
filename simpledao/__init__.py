# inspiration from odoo/api.py, odoo/modules/registry.py
from . import environment
from . import db

cr = env = False
def init(config):
    global cr, env
    cr = db.Cursor(config)

    env = environment.Environment()
    env.init(config)
