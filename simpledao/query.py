# this cleaner version of odoo/osv/expression.py
# really need to override / change the logical
# not supporting unaccent
# not supporting hierarchy operator
# this core logic is converting from polish (prefix) notation into infix notation
# then convert the infix notation into sql
# the origin http://christophe-simonis-at-tiny.blogspot.com/2008/08/new-new-domain-notation.html

NOT_OPERATOR = '!'
OR_OPERATOR = '|'
AND_OPERATOR = '&'
DOMAIN_OPERATORS = (NOT_OPERATOR, OR_OPERATOR, AND_OPERATOR)

DOMAIN_OPERATORS_NEGATION = {
    AND_OPERATOR: OR_OPERATOR,
    OR_OPERATOR: AND_OPERATOR,
}

# only this operator are valid, remove <>, child_of, parent_of
TERM_OPERATORS = ('=', '!=', '<=', '<', '>', '>=', '=?', '=like', '=ilike',
                  'like', 'not like', 'ilike', 'not ilike', 'in', 'not in')

TERM_OPERATORS_NEGATION = {
    '<': '>=',
    '>': '<=',
    '<=': '>',
    '>=': '<',
    '=': '!=',
    '!=': '=',
    'in': 'not in',
    'like': 'not like',
    'ilike': 'not ilike',
    'not in': 'in',
    'not like': 'like',
    'not ilike': 'ilike',
}

# NEGATIVE_TERM_OPERATORS = ('!=', 'not like', 'not ilike', 'not in')

TRUE_LEAF = (1, '=', 1)
FALSE_LEAF = (0, '=', 1)

# leaf section
# check if the leaf / domains element is valid
def is_leaf(element):
    """ Test whether an object is a valid domain term:
        - is a list or tuple
        - with 3 elements
        - second element if a valid op

        :param tuple element: a leaf in form (left, operator, right)

        Note: OLD TODO change the share wizard to use this function.
    """
    return (isinstance(element, tuple) or isinstance(element, list)) \
        and len(element) == 3 \
        and element[1] in TERM_OPERATORS \
        and ((isinstance(element[0], str) and element[0])
             or tuple(element) in (TRUE_LEAF, FALSE_LEAF))

# normalize domain element
def normalize_leaf(element):
    """ Change a term's operator to some canonical form, simplifying later
        processing. """
    if not is_leaf(element):
        return element
    left, operator, right = element
    operator = operator.lower()
    if isinstance(right, bool) and operator in ('in', 'not in'):
        operator = '=' if operator == 'in' else '!='
    if isinstance(right, (list, tuple)) and operator in ('=', '!='):
        operator = 'in' if operator == '=' else 'not in'
    return left, operator, right

def is_boolean(element):
    return element == TRUE_LEAF or element == FALSE_LEAF

def is_operator(element):
    """ Test whether an object is a valid domain operator. """
    return isinstance(element, str) and element in DOMAIN_OPERATORS

def check_leaf(element):
    if not is_operator(element) and not is_leaf(element):
        raise Exception("Invalid leaf %s" % str(element))

# domain section
# taken from odoo normalize_domain
# this is where arity of the domain operator formatted
def normalize_domain(domain):
    """Returns a normalized version of ``domain_expr``, where all implicit '&' operators
       have been made explicit. One property of normalized domain expressions is that they
       can be easily combined together as if they were single domain components.
    """
    assert isinstance(domain, (list, tuple)), "Domains to normalize must have a 'domain' form: a list or tuple of domain components"
    if not domain:
        return [TRUE_LEAF]
    result = []
    expected = 1                            # expected number of expressions
    op_arity = {NOT_OPERATOR: 1, AND_OPERATOR: 2, OR_OPERATOR: 2}
    for token in domain:
        if expected == 0:                   # more than expected, like in [A, B]
            result[0:0] = [AND_OPERATOR]             # put an extra '&' in front
            expected = 1
        if isinstance(token, (list, tuple)):  # domain term
            expected -= 1
            token = tuple(token)
        else:
            expected += op_arity.get(token, 0) - 1
        result.append(token)
    assert expected == 0, 'This domain is syntactically not correct: %s' % (domain)
    return result

# taken from odoo distribute_not
def distribute_not(domain):
    """ Distribute any '!' domain operators found inside a normalized domain.

    Because we don't use SQL semantic for processing a 'left not in right'
    query (i.e. our 'not in' is not simply translated to a SQL 'not in'),
    it means that a '! left in right' can not be simply processed
    by __leaf_to_sql by first emitting code for 'left in right' then wrapping
    the result with 'not (...)', as it would result in a 'not in' at the SQL
    level.

    This function is thus responsible for pushing any '!' domain operators
    inside the terms themselves. For example::

         ['!','&',('user_id','=',4),('partner_id','in',[1,2])]
            will be turned into:
         ['|',('user_id','!=',4),('partner_id','not in',[1,2])]

    """

    # This is an iterative version of a recursive function that split domain
    # into subdomains, processes them and combine the results. The "stack" below
    # represents the recursive calls to be done.
    result = []
    stack = [False]

    for token in domain:
        negate = stack.pop()
        # negate tells whether the subdomain starting with token must be negated
        if is_leaf(token):
            if negate:
                left, operator, right = token
                if operator in TERM_OPERATORS_NEGATION:
                    result.append((left, TERM_OPERATORS_NEGATION[operator], right))
                else:
                    result.append(NOT_OPERATOR)
                    result.append(token)
            else:
                result.append(token)
        elif token == NOT_OPERATOR:
            stack.append(not negate)
        elif token in DOMAIN_OPERATORS_NEGATION:
            result.append(DOMAIN_OPERATORS_NEGATION[token] if negate else token)
            stack.append(negate)
            stack.append(negate)
        else:
            result.append(token)

    return result

class Builder(object):

    def __init__(self, domain, model):
        attrs = model.attrs
        self.model = attrs.get("model")
        self.primary_key = attrs.get("primary")
        self.query = ""
        self.expression = distribute_not(normalize_domain(domain))
        self.parse()

    def __leaf_to_sql(self, leaf):
        left, operator, right = leaf

        # final sanity checks - should never fail
        assert operator in TERM_OPERATORS, \
            "Invalid operator %r in domain term %r" % (operator, leaf)
        assert leaf in (TRUE_LEAF, FALSE_LEAF) or isinstance(left, str), \
            "Invalid field %r in domain term %r" % (left, leaf)

        if leaf == TRUE_LEAF:
            query = 'TRUE'
            params = []

        elif leaf == FALSE_LEAF:
            query = 'FALSE'
            params = []

        elif operator in ['in', 'not in']:
            # Two cases: right is a boolean or a list. The boolean case is an
            # abuse and handled for backward compatibility.
            if isinstance(right, bool):
                if (operator == 'in' and right) or (operator == 'not in' and not right):
                    query = '("%s" IS NOT NULL)' % (left)
                else:
                    query = '("%s" IS NULL)' % (left)
                params = []
            elif isinstance(right, (list, tuple)):
                params = [it for it in right if it != False]
                if params:
                    instr = ','.join(['%s'] * len(params))
                    query = '("%s" %s (%s))' % (left, operator, instr)
                else:
                    # The case for (left, 'in', []) or (left, 'not in', []).
                    query = 'FALSE' if operator == 'in' else 'TRUE'
                
                # might not need this check
                # check_null = len(params) < len(right)
                # if (operator == 'in' and check_null) or (operator == 'not in' and not check_null):
                #     query = '(%s OR "%s" IS NULL)' % (query, left)
                # elif operator == 'not in' and check_null:
                #     query = '(%s AND "%s" IS NOT NULL)' % (query, left)
            else:  # Must not happen
                raise Exception("Invalid domain term %r" % (leaf,))

        # left in model and model._fields[left].type == "boolean" 
        # isinstance(left, str) for flagging only
        # elif isinstance(left, str) and ((operator == '=' and right is False) or (operator == '!=' and right is True)):
        #     query = '("%s" IS NULL or "%s" = false )' % (left, left)
        #     params = []

        elif (right is False or right is None) and (operator == '='):
            query = '"%s" IS NULL ' % (left)
            params = []

        # left in model and model._fields[left].type == "boolean"
        # isinstance(left, str) for flagging only 
        # elif isinstance(left, str) and ((operator == '!=' and right is False) or (operator == '==' and right is True)):
        #     query = '("%s" IS NOT NULL and "%s" != false)' % (left, left)
        #     params = []

        elif (right is False or right is None) and (operator == '!='):
            query = '"%s" IS NOT NULL' % (left)
            params = []

        elif operator == '=?':
            if right is False or right is None:
                # '=?' is a short-circuit that makes the term TRUE if right is None or False
                query = 'TRUE'
                params = []
            else:
                # '=?' behaves like '=' in other cases
                query, params = self.__leaf_to_sql((left, '=', right))

        else:
            need_wildcard = operator in ('like', 'ilike', 'not like', 'not ilike')
            sql_operator = {'=like': 'like', '=ilike': 'ilike'}.get(operator, operator)
            cast = '::text' if  sql_operator.endswith('like') else ''
            column = '"%s"%s' % (left, cast)
            query = '(%s %s %s)' % (column, sql_operator, "%s")

            # might not need this check
            # if (need_wildcard and not right) or (right and operator in NEGATIVE_TERM_OPERATORS):
            #     query = '(%s OR "%s" IS NULL)' % (query, left)

            if need_wildcard:
                params = ['%%%s%%' % str(right)]
            else:
                params = [right]

        return query, params

    def parse(self):
        stack = []
        for leaf in self.expression:
            leaf = normalize_leaf(leaf)
            check_leaf(leaf)
            stack.append(leaf)

        result = []
        while stack:
            leaf = stack.pop()
            if is_operator(leaf):
                if leaf == NOT_OPERATOR:
                    expr, params = result.pop()
                    result.append('(NOT (%s))' % expr, params)
                else:
                    ops = {AND_OPERATOR: '(%s AND %s)', OR_OPERATOR: '(%s OR %s)'}
                    lhs, lhs_params = result.pop()
                    rhs, rhs_params = result.pop()
                    result.append([ops[leaf] % (lhs, rhs), lhs_params + rhs_params])
                continue

            expr, params = self.__leaf_to_sql(leaf)
            result.append([expr, params])
        
        self.result = result[0]

    def to_sql(self):
        return self.result

# TODO: sparate the testing file into the testing file
# testing purpose

# class s(): attrs = {"model": "book", "primary": "book_id"}

# res = Builder([
#     "!", "|", "&",
#     ("user_id", "=", "dhimas"),
#     ("user_id", '!=', "rudi"),
#     "&",
#     ("status", "=", "draft"),
#     ("status", '!=', "ready")
# ], s)

# res = Builder([
#    ("data", "not in", ["a", "b", "c"])
# ], s)

# res = Builder([
#    ("data", "in", ["a", "b", "c"]),
#    ("active", "=", True),
# ], s)