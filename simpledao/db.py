# simple cursor reimplementation odoo/sql_db.py
# if this implemented with wsgi / asgi server
# need to change the implementation to per model cursor or other way
import psycopg2
from psycopg2 import pool
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT, ISOLATION_LEVEL_READ_COMMITTED, ISOLATION_LEVEL_REPEATABLE_READ

# only expose specific function / class / var
__all__ = [
    "Cursor",
]

# odoo currently use serialize iso / read commited
# check the docs for the commited
# https://www.postgresql.org/docs/current/transaction-iso.html
class Cursor(object):
    
    def __init__(self, config, serialized=True):
        self._closed = True
        self._serialized = serialized 

        self._cnx = psycopg2.connect(
            user=config.get("user"),
            password=config.get("password"),
            host=config.get("host"),
            port=config.get("port"),
            database=config.get("database")
        )
        self._obj = self._cnx.cursor()

        self._closed = False   # real initialisation value
        self.autocommit(False)

    def execute(self, query, params=None):
        res = self._obj.execute(query, params)
        return res

    def __build_dict(self, row):
        return {d.name: row[i] for i, d in enumerate(self._obj.description)}

    def fetchone(self):
        return self._obj.fetchone()

    def fetchmany(self, size):
        return self._obj.fetchmany(size)

    def fetchall(self):
        return self._obj.fetchall()

    def close(self):
        if self._obj:
            self._obj.close()

        del self._obj

        if self._cnx:
            self._cnx.rollback()
    
    def autocommit(self, on):
        if on:
            isolation_level = ISOLATION_LEVEL_AUTOCOMMIT
        else:
            isolation_level = ISOLATION_LEVEL_REPEATABLE_READ
            if self._serialized:
                isolation_level = ISOLATION_LEVEL_READ_COMMITTED
        self._cnx.set_isolation_level(isolation_level)

    def commit(self):
        result = False
        if self._cnx:
            result = self._cnx.commit()
        return result

    def rollback(self):
        result = False
        if self._cnx:
            result = self._cnx.rollback()
        return result

    # def __enter__(self):
    #     return self

    # def __exit__(self, exc_type, exc_value, traceback):
    #     if exc_type is None:
    #         self.commit()
    #     self.close()