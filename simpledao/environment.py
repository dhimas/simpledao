import re
from collections.abc import Mapping
from . import db

from . import query

class Environment(Mapping):
    attrs = {}

    def init(self, config):
        self._cr = db.Cursor(config)

    def __getitem__(self, key):
        keys = key.split("|")
        if len(keys) != 2:
            raise Exception("wrong key, table_name|table_primary_key expected")
        self.attrs.update({
            "model": keys[0],
            "primary": keys[1]
        })
        return self

    def __len__(self):
        return len(self.attrs)

    def __iter__(self):
        return iter(self.attrs)

    def _where_calc(self, domain):
        return query.Builder(domain, self).to_sql()
    
    def _order_calc(self, order):
        order_by_clause = ''
        if order:
            regex_order = re.compile(r'^(\s*([a-z0-9:_]+|"[a-z0-9:_]+")(\s+(desc|asc))?\s*(,|$))+(?<!,)$', re.I)
            if not regex_order.match(order):
                raise Exception("order value is invalid")
                
            order_by_elements = []
            for order_part in order.split(','):
                order_split = order_part.strip().split(' ')
                order_field = order_split[0].strip()
                order_direction = order_split[1].strip().upper() if len(order_split) == 2 else ''
                order_by_elements.append('"%s" %s' % (order_field, order_direction))

            if order_by_elements:
                order_by_clause = ",".join(order_by_elements)

        return order_by_clause and (' ORDER BY %s ' % order_by_clause) or ''

    def _search(self, domain, fields=[], offset=0, limit=None, order=None, count=False):
        if not isinstance(fields, list):
            raise Exception("fields must be list")
        model = self.attrs.get("model")
        default_fields = self.attrs.get("primary")
        where_clause, where_clause_params = self._where_calc(domain)
        order_str = self._order_calc(order)
        limit_str = limit and " limit %d" % limit or ""
        offset_str = offset and " offset %d" % offset or ""
        if count:
            query = "SELECT COUNT(1) FROM (SELECT 1 FROM %s WHERE %s%s%s) sub" % (model, where_clause, limit_str, offset_str)
            self._cr.execute(query, where_clause_params)
            return self._cr.fetchone()[0]
        else:
            if not fields:
                fields = []
            if default_fields in fields:
                fields.remove(default_fields)
            temp_fields = []
            for field in fields:
                if field not in temp_fields:
                    temp_fields.append(field)
            fields = temp_fields
            fields.insert(0, default_fields)
            where_clause, where_clause_params = self._where_calc(domain)
            query = "SELECT %s FROM %s WHERE %s%s%s%s" % (",".join(fields), model, where_clause, order_str, limit_str, offset_str)
            self._cr.execute(query, where_clause_params)
            res = []
            while True:
                data = self._cr.fetchone()
                if data:
                    res.append({fields[i]:data[i] for i in range(len(data))})
                else:
                    break
            return res
    
    def search(self, domain, fields, offset=0, limit=None, order=None):
        return self._search(domain, fields, offset=offset, limit=limit, order=order)

    def search_count(self, domain, offset=0, limit=None, order=None):
        return self._search(domain, offset=offset, limit=limit, order=order, count=True)

    def create(self, vals_list):
        if not isinstance(vals_list, list):
            raise Exception("vals must be list")
        
        quote = '"{}"'.format

        ids = []
        for vals in vals_list:
            query = "INSERT INTO {} ({}) VALUES ({}) RETURNING {}".format(
                quote(self.attrs.get('model')),
                ", ".join(quote(key) for key in vals),
                ", ".join('%s' for key in vals),
                quote(self.attrs.get('primary')),
            )
            self._cr.execute(query, [vals.get(key) for key in vals])
            ids.append(self._cr.fetchone()[0])
        self._cr.commit()
        return ids
    
    def write(self, ids, vals):
        if not isinstance(ids, list):
            raise Exception("ids must be list of id")

        if not isinstance(vals, dict):
            raise Exception("vals must be dict")

        quote = '"{}"'.format

        query = "UPDATE {} SET {} WHERE {}".format(
            quote(self.attrs.get('model')),
            ", ".join("{}={}".format(quote(key), '%s') for key in vals),
            "{} IN {}".format(quote(self.attrs.get('primary')), "%s"),
        )
        params = [vals.get(key) for key in vals]
        params.append(tuple(ids))
        self._cr.execute(query, params)
        self._cr.commit()

        return True

    def unlink(self, ids):
        if not isinstance(ids, list):
            raise Exception("ids must be list of id")
        
        quote = '"{}"'.format

        query = "DELETE FROM {} WHERE {}".format(
            quote(self.attrs.get('model')),
            "{} IN {}".format(quote(self.attrs.get('primary')), "%s"),
        )
        params = []
        params.append(tuple(ids))
        self._cr.execute(query, params)
        self._cr.commit()

        return True
