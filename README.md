# simpledao
simple data access object inspired from odoo orm
<br>
based on [PL/SQL Challenge](https://docs.google.com/document/d/1Pa_GsxilGW-FRylh304kAv7itNElLVxS3yr3mzjJSBA/edit)


# roadmap
- implement runtime cursor getter, currently once the function called its refere to first initiated cursor in the pool
- implement variable typing to easy access function of exposed object
- make this module available in pypy


# test
configure and run test.py